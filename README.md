### Como rodar o projeto?

- Criar um banco de dados no Postgres
- Setar as variáveis de acordo com as configuração do seu banco no arquivo `.env`:\
TYPEORM_USERNAME = \
TYPEORM_PASSWORD = \
TYPEORM_DATABASE =

- Após isso, dentro da pasta do projeto executar os seguintes comandos:\
`$ yarn install` \
`$ yarn typeorm migration:run` \
`$ yarn dev` 

- Os endpoints da API ficam na pasta `src/routes.ts`. Você pode utilizar o Postman, Insomnia ou seu simulador preferido de requisições para fazer os testes.
