import express from 'express';
import { routes } from './routes';
import './database';

const app = express();
const port = process.env.PORT || 3002;

app.use(express.json());
app.use(routes);
app.listen(port, () => console.log(`Server is runing on port ${port}`));
