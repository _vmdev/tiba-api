import { Router } from "express";
import { CategoryController } from "./controllers/CategoryController";
import { ProductController } from "./controllers/ProductController";

const categoryController = new CategoryController();
const productController = new ProductController();
const routes = Router();

// rotas de categoria
routes.get('/category', categoryController.index );
routes.get('/category/:id', categoryController.show );
routes.post('/category', categoryController.create );
routes.put('/category/:id', categoryController.update );
routes.delete('/category/:id', categoryController.delete );

// rotas de produto
routes.get('/product', productController.index );
routes.get('/product/:id', productController.show );
routes.post('/product', productController.create );
routes.put('/product/:id', productController.update );
routes.delete('/product/:id', productController.delete );

export { routes }