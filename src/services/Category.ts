import { getRepository } from 'typeorm'
import { Category } from '../entities/Category';

type CategoryRequest = {
	id?: string;
	name: string;
	description: string;
	status: boolean;
}

export class CategoryService {
	async index() {
		const repo = getRepository(Category);
		const categories = await repo.find()

		return categories;
	}

	async show(id: string) {
		const repo = getRepository(Category);

		const category  = await repo.findOne(id);
		
		if(!category) {
			return new Error('Category does not exists');
		}

		return category;
	}

	async create( {name, description, status}:CategoryRequest ): Promise<Category | Error> {
		const repo = getRepository(Category);

		if(await repo.findOne({name})) {
			return new Error('Category already exists');
		}

		const category = repo.create({
			name,
			description,
			status
		});

		await repo.save(category);

		return category;
	}

	async update( {id, name, description, status}:CategoryRequest ): Promise<Category | Error> {
		const repo = getRepository(Category);

		const category  = await repo.findOne(id);
		
		if(!category) {
			return new Error('Category does not exists');
		}

		category.name = name ? name : category.name;
		category.description = description ? description : category.description;
		category.status = status ? status : category.status;

		await repo.save(category);

		return category;
	}

	async delete(id: string) {
		const repo = getRepository(Category);

		if(!await repo.findOne(id)) {
			return new Error('Category does not exists');
		}

		await repo.delete(id);
	}
}