import { getRepository } from 'typeorm'
import { Category } from '../entities/Category'
import { Product } from '../entities/Product';

type ProductRequest = {
	id?: string;
	name: string;
	description: string;
	quantity: number;
	price: number;
	is_active: boolean;
	category_id: string;
}

export class ProductService {
	async index() {
		const repo = getRepository(Product);
		const products = await repo.find()

		return products;
	}

	async show(id: string) {
		const repo = getRepository(Product);

		const product  = await repo.findOne(id);
		
		if(!product) {
			return new Error('Product does not exists');
		}

		return product;
	}

	async create( {name, description, quantity, price, is_active, category_id}:ProductRequest ): Promise<Product | Error> {
		const repo = getRepository(Product);
		const repoCategory = getRepository(Category)

		if(!await repoCategory.findOne(category_id)) {
			return new Error('Category does nmot exists');
		}

		const product = repo.create({
			name,
			description,
			quantity,
			price,
			is_active,
			category_id
		});

		await repo.save(product);

		return product;
	}

	async update( {id, name, description, quantity, price, is_active, category_id}:ProductRequest ): Promise<Product | Error> {
		const repo = getRepository(Product);

		const product  = await repo.findOne(id);
		
		if(!product) {
			return new Error('Product does not exists');
		}

		product.name = name ? name : product.name;
		product.description = description ? description : product.description;
		product.quantity = quantity ? quantity : product.quantity;
		product.price = price ? price : product.price;
		product.is_active = is_active ? is_active : product.is_active;

		await repo.save(product);

		return product;
	}

	async delete(id: string) {
		const repo = getRepository(Product);

		if(!await repo.findOne(id)) {
			return new Error('Product does not exists');
		}

		await repo.delete(id);
	}
}