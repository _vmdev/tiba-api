import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class CreateProducts1642353624353 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'products',
                columns: [
                    {
                        name: 'id',
                        type: 'uuid',
                        isPrimary: true
                    },
                    {
                        name: 'name',
                        type: 'varchar',
                        isUnique:true
                    },
                    {
                        name: 'description',
                        type: 'varchar'
                    },
                    {
                        name: 'quantity',
                        type: 'int'
                    },
                    {
                        name: 'price',
                        type: 'decimal',
                        precision: 8,
                        scale: 2,
                        default: 0
                    },
                    {
                        name: 'is_active',
                        type: 'boolean'
                    },
                    {
                        name: 'category_id',
                        type: 'uuid'
                    },
                    {
                        name: 'created_at',
                        type: 'timestamp',
                        default: 'now()'
                    }
                ],
                foreignKeys: [
                    {
                        name: "fk_products_category",
                        columnNames: ['category_id'],
                        referencedTableName: 'categories',
                        referencedColumnNames: ['id']
                    }
                ]
            })
        )
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('products')
    }

}
