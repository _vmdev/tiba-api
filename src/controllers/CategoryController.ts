import { Request, Response } from "express";
import { CategoryService } from "../services/Category";

export class CategoryController  {
	async index(request: Request, response: Response) {
		const service = new CategoryService();
		const categories = await service.index();

		return response.json(categories);
	}
	async show(request: Request, response: Response) {
		const { id } = request.params;
		const service = new CategoryService();
		const result = await service.show(id);

		if (result instanceof Error) {
			return response.status(400).json(result.message);
		}

		return response.json(result)
	}
	async create(request: Request, response: Response) {
		const { name, description, status } = request.body;

		const service = new CategoryService();
		const result  = await service.create({name, description, status});
		
		if (result instanceof Error) {
			return response.status(400).json(result.message);
		}

		return response.json(result)
	}
	async update(request: Request, response: Response) {
		const { id } = request.params;
		const { name, description, status } = request.body;
		const service = new CategoryService();
		const result = await service.update({id, name, description, status});

		if(result instanceof Error) {
			return response.status(400).json(result.message);
		}

		return response.json(result);
	}
	async delete(request: Request, response: Response) {
		const { id } = request.params;
		const service = new CategoryService();
		const result = await service.delete(id);

		if(result instanceof Error) {
			return response.status(400).json(result.message);
		}

		return response.status(204).end();
	}
}