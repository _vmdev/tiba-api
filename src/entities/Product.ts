import { Entity, Column, CreateDateColumn, PrimaryColumn, ManyToOne, JoinColumn } from "typeorm";
import { v4 as uuid } from 'uuid';
import { Category  } from "./Category";


@Entity('products')
export class Product {

	@PrimaryColumn()
	id: string;

	@Column()
	name: string;

	@Column()
	description: string;
	
	@Column()
	quantity: number;

	@Column({ type: 'decimal', precision: 5, scale: 2, default: 0 })
	price: number;

	@Column()
	is_active: boolean;
	
	@ManyToOne(() => Category)
	@JoinColumn({ name: 'category_id' })
	category_id: Category;

	@CreateDateColumn()
	created_at: Date;

	constructor() {
		if(!this.id) {
			this.id = uuid();
		}
	}
}