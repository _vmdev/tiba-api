import { Entity, Column, CreateDateColumn, PrimaryColumn } from "typeorm";
import { v4 as uuid } from 'uuid';

@Entity('categories')
export class Category {

	@PrimaryColumn()
	id: String;

	@Column()
	name: String;

	@Column()
	description: String;

	@Column()
	status: Boolean;

	@CreateDateColumn()
	created_at: Date;

	constructor() {
		if(!this.id) {
			this.id = uuid();
		}
	}
}